﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using Application.Expenses.Models;
using Application.Expenses.Commands;
using Application.Expenses.Queries.GetAllExpenses;
using Application.Expenses.Queries.GetExpense;
using Application.Expenses.Commands.PayExpense;
using Application.Expenses.Queries.GetMyExpenses;
using Application.Expenses.Queries.GetExpensesToPay;
using Application.Expenses.Queries.FilterAdminExpenses;
using Application.Expenses.Queries.FilterPersonInNeedExpenses;
using Application.Expenses.Queries.FilterPayerExpenese;
using Stripe.Checkout;
using Stripe;
using Application.Expenses.Queries.GetPayedExpenses;
using Application.Expenses.Commands.UpdateExpense;

namespace WebUI.Controllers
{
    public class ExpenseController : ApiController
    {
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ExpenseModel>>> GetAll()
        {
            return Ok(await Mediator.Send(new GetAllExpensesQuery()));
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<ExpenseModel>> Get(int id)
        {
            return Ok(await Mediator.Send(new GetExpenseQuery { Id = id }));
        }

        [HttpGet("filter/admin/payed={payedFilter}&expenseOWener={expenseOwner}")]
        public async Task<ActionResult<IEnumerable<ExpenseModel>>> FilterAdminExpenses(string payedFilter, string expenseOwner)
        {
            return Ok(await Mediator.Send(new FilterAdminExpensesQuery { PayedFilter = payedFilter, ExpenseOwener = expenseOwner }));
        }

        [HttpPost("pay-expense")]
        public async Task<IActionResult> PayExpense(string nesto, string nesto2)
        {
            var email = nesto;
            var token = nesto2;
            return Ok();
        }

        [HttpPost("create-checkout-session")]
        public ActionResult Create(string request)
        {
            var paymentIntents = new PaymentIntentService();
            var paymentIntent = paymentIntents.Create(new PaymentIntentCreateOptions
            {
                Amount = 100,
                Currency = "usd",
            });
            return Ok(new { clientSecret = paymentIntent.ClientSecret });
        }

        [HttpGet("filter/person-in-need/payed={payedFilter}")]
        public async Task<ActionResult<IEnumerable<ExpenseModel>>> FilterPersonInNeedExpenses(string payedFilter)
        {
            return Ok(await Mediator.Send(new FilterPersonInNeedExpensesQuery { PayedFilter = payedFilter }));
        }

        [HttpGet("filter/payer/expenseOWener={expenseOwner}")]
        public async Task<ActionResult<IEnumerable<ExpenseModel>>> FilterPayerExpenses(string expenseOwner)
        {
            return Ok(await Mediator.Send(new FilterPayerExpeneseQuery { ExpenseOwener = expenseOwner }));
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesDefaultResponseType]
        public async Task<IActionResult> Create(CreateExpenseCommand command)
        {
            await Mediator.Send(command);
            return NoContent();
        }

        [HttpPost("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesDefaultResponseType]
        public async Task<IActionResult> Update(UpdateExpenseCommand command)
        {
            await Mediator.Send(command);
            return NoContent();
        }

        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> PayExpense(int id, PayExpenseCommand command)
        {
            if (id != command.Id)
            {
                return BadRequest();
            }
            await Mediator.Send(command);
            return NoContent();
        }

        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Delete(int id)
        {
            await Mediator.Send(new DeleteExpenseCommand { Id = id });
            return NoContent();
        }

        [HttpGet("my-expenses")]
        public async Task<ActionResult<IEnumerable<ExpenseModel>>> GetMyExpenses()
        {
            return Ok(await Mediator.Send(new GetMyExpensesQuery()));
        }

        [HttpPost("my-expenses")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesDefaultResponseType]
        public async Task<IActionResult> CreatePersonalExpense(CreatePersonalExpenseCommand command)
        {
            await Mediator.Send(command);
            return NoContent();
        }

        [HttpGet("expenses-to-pay")]
        public async Task<ActionResult<IEnumerable<ExpenseModel>>> GetExpensesToPay()
        {
            return Ok(await Mediator.Send(new GetExpensesToPayQuery()));
        }

        [HttpGet("payer/get-payed-expenses")]
        public async Task<ActionResult<IEnumerable<ExpenseModel>>> GetPayedExpenses()
        {
            return Ok(await Mediator.Send(new GetPayedExpensesQuery()));
        }


    }
}
