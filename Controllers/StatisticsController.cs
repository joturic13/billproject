﻿using Application.Statistics.Models;
using Application.Statistics.Queries.GetExpenseStatistics;
using Application.Statistics.Queries.GetPeopleInNeedStatistics;
using Application.Statistics.Queries.GetStatisticsForPayer;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
namespace WebUI.Controllers
{
    public class StatisticsController : ApiController
    {
        [HttpGet]
        public async Task<ActionResult<IEnumerable<StatisticsModel>>> GetAll()
        {
            var expensesStatistics = await Mediator.Send(new GetExpenseStatistiscQuery());
            var payerStatistics = await Mediator.Send(new GetPayersStatisticsQuery());
            var peopleInNeedStatistics = await Mediator.Send(new GetPeopleInNeedStatisticsQuery());

            return Ok(new StatisticsModel(expensesStatistics, payerStatistics, peopleInNeedStatistics));
        }

        [HttpGet("payer")]
        public async Task<ActionResult<IEnumerable<StatisticsModel>>> GetStatisticForPayer()
        {
            var expensesStatistics = await Mediator.Send(new GetStatisticsForPayerQuery());

            return Ok(new StatisticsModel(expensesStatistics));
        }
    }
}
