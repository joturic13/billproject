﻿using Application.PeopleInNeed.Commands.CreatePersonInNeed;
using Application.PeopleInNeed.Commands.DeletePersonInNeed;
using Application.PeopleInNeed.Commands.UpdatePersonInNeed;
using Application.PeopleInNeed.Models;
using Application.PeopleInNeed.Queries.GetAllPeopleInNeed;
using Application.PeopleInNeed.Queries.GetPersonInNeed;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace WebUI.Controllers
{
    public class PersonInNeedController: ApiController
    {
        [HttpGet]
        public async Task<ActionResult<IEnumerable<PersonInNeedModel>>> GetAll()
        {
            return Ok(await Mediator.Send(new GetAllPeopleInNeedQuery()));
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<PersonInNeedModel>> Get(string id)
        {
            return Ok(await Mediator.Send(new GetPersonInNeedQuery { Id = id }));
        }

        [HttpPost("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesDefaultResponseType]
        public async Task<IActionResult> Update(UpdatePersonInNeedCommand command)
        {
            await Mediator.Send(command);
            return NoContent();
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesDefaultResponseType]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Create(CreatePersonInNeedCommand command)
        {
            await Mediator.Send(command);
            return NoContent();
        }

        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Delete(string id)
        {
            await Mediator.Send(new DeletePersonInNeedCommand { Id = id });
            return NoContent();
        }


    }
}
