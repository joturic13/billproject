﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Application.Identity.Commands.Login;
using System.Threading.Tasks;

namespace WebUI.Controllers
{
    [Authorize]
    public class AccountController : ApiController
    {
        [HttpPost("login")]
        [AllowAnonymous]
        public async Task<string> Login(LoginCommand command)
        {
            return await Mediator.Send(command);
        }    
    }

}
