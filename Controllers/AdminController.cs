﻿using Application.Admins.Models;
using Application.Admins.Queries.GetAdmin;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Application.Admins.Commands.CreateAdmin;

namespace WebUI.Controllers
{
    [Authorize(Roles = "Admin")]
    public class AdminController: ApiController
    {

        [HttpGet("{id}")]
        public async Task<ActionResult<AdminModel>> Get(string id)
        {
            return Ok(await Mediator.Send(new GetAdminQuery { Id = id }));
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesDefaultResponseType]
        [AllowAnonymous]
        public async Task<IActionResult> Create(CreateAdminCommand command)
        {
            await Mediator.Send(command);
            return NoContent();
        }
    }
}
