﻿using Application.Payers.Commands.CreatePayer;
using Application.Payers.Models;
using Application.Payers.Queries.GetAllPayers;
using Application.Payers.Queries.GetPayer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace WebUI.Controllers
{
    [AllowAnonymous]
    public class PayerController : ApiController
    {
        [HttpGet]
        public async Task<ActionResult<IEnumerable<PayerModel>>> GetAll()
        {
            return Ok(await Mediator.Send(new GetAllPayersQuery()));
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<PayerModel>> Get(string id)
        {
            return Ok(await Mediator.Send(new GetPayerQuery { Id = id }));
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesDefaultResponseType]
        public async Task<IActionResult> Create(CreatePayerCommand command)
        {
            return Ok(await Mediator.Send(command));
        }
    }
}
