import React from "react";
import { ExpenseEnum } from "../constants/enums/expenseEnum";

export function getValueFromEnum(value: string): string {
  var list = Object.values(ExpenseEnum);
  var index = list.findIndex((e) => e.value == value);
  return list[index].name;
}
