import React from "react";
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import styles from "./styles.module.scss";
import Checkbox from "@material-ui/core/Checkbox";
import Link from "@material-ui/core/Link";
import Grid from "@material-ui/core/Grid";
import UserApi from "../../../service/UserService/UserService";
import { Formik } from "formik";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import * as Yup from "yup";
import Container from "@material-ui/core/Container";
import TextInput from "../../Roles/Common/TextInput/TextInput";
import { parseAuthToken } from "../../../helpers/AccountHelper";
import { LoginResponse } from "../../../types/ILoginResponse";
import { APP } from "../../../constants/appRoutes";

const useStyles = makeStyles((theme: any) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    backgroundColor: "white",
    padding: "26px",
    borderRadius: "16px",
  },
  highInput: {
    input: {
      height: "45px !important",
    },
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

interface IProps {
  history: any;
}

const CompleteProfile: React.FC<IProps> = (props: IProps) => {
  const classes = useStyles();

  const handleLoginSuccess = (loginResponse: LoginResponse) => {
    if (loginResponse.role === "Admin") {
      props.history.push(APP.ROLES.ADMIN.DASHBOARD);
    } else if (loginResponse.role === "PersonInNeed") {
      props.history.push(APP.ROLES.PERSON_IN_NEED.LIST_OF_EXPENSES);
    } else if (loginResponse.role === "Payer") {
      props.history.push(APP.ROLES.PAYER.LIST_OF_EXPENSES);
    }
  };

  const Login = async (email: string, password: string) => {
    window.localStorage.removeItem("authToken");
    const authToken = await UserApi.Login({ email, password });
    const loginResponse = parseAuthToken(authToken);
    window.localStorage.setItem("authToken", loginResponse.token);
    handleLoginSuccess(loginResponse);
  };

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Complete your profile!
        </Typography>
        <Formik
          initialValues={{
            email: "",
            password: "",
          }}
          onSubmit={(values: any) => Login(values.email, values.password)}
        >
          {(props: any) => {
            const {
              touched,
              errors,
              isSubmitting,
              handleChange,
              handleBlur,
              handleSubmit,
            } = props;
            return (
              <form
                className={classes.form + " " + styles.highInput}
                onSubmit={handleSubmit}
              >
                <div className={styles.inputHolder}>
                  <TextInput
                    handleChange={handleChange}
                    handleBlur={handleBlur}
                    id="email"
                    errors={errors}
                    touched={touched}
                    value={props.values && props.values.email}
                    placeholder="Email"
                  />
                </div>
                <div className={styles.inputHolder}>
                  <TextInput
                    handleChange={handleChange}
                    handleBlur={handleBlur}
                    id="password"
                    type="password"
                    errors={errors}
                    touched={touched}
                    value={props.values && props.values.password}
                    placeholder="Lozinka"
                  />
                </div>
                <FormControlLabel
                  control={<Checkbox value="remember" color="primary" />}
                  label="Remember me"
                />
                <Button
                  type="submit"
                  fullWidth
                  disabled={isSubmitting}
                  variant="contained"
                  color="primary"
                  className={classes.submit}
                >
                  Prijava
                </Button>
                <Grid container>
                  <Grid item xs>
                    <Link href="/forgot-password" variant="body2">
                      Zaboravljena lozinka?
                    </Link>
                  </Grid>
                  <Grid item>
                    <Link href="/register" variant="body2">
                      {"Nemate račun? Registrirajte se"}
                    </Link>
                  </Grid>
                </Grid>
              </form>
            );
          }}
        </Formik>
      </div>
    </Container>
  );
};

export default CompleteProfile;
