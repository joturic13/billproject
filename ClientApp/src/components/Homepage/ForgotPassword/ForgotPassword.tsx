import React, { useState } from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import mainStyles from '../../../styles.module.scss';

const useStyles = makeStyles((theme: any) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    backgroundColor: 'white',
    padding: '26px',
    borderRadius: '16px'
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

const ForgotPassword = () => {
  const classes = useStyles();
    const [passwordResset, setPasswordReset] = useState(false);

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
         Zaboravljena lozinka 
        </Typography>
        {!passwordResset? <>
        <form className={classes.form} noValidate>
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="email"
            label="Email adresa"
            name="email"
            autoComplete="email"
            autoFocus
          />          
          <Button
            onClick={()=> setPasswordReset(true)}
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
          >
            Resetiraj lozinku
          </Button>
        </form>
        </>
        :
        <>
        <CssBaseline />
         <Typography component="h5" variant="subtitle2" className={mainStyles.marginTopS}>
         Poslali smo vam na mail zahtjev za promjenu lozinke. 
        </Typography>
        </>}
      </div>
    </Container>
  );
}

export default ForgotPassword;