import React, { useContext } from "react";
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import TextField from "@material-ui/core/TextField";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import styles from "./styles.module.scss";
import { Formik } from "formik";
import * as Yup from "yup";
import Link from "@material-ui/core/Link";
import Grid from "@material-ui/core/Grid";
import Box from "@material-ui/core/Box";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import UserApi from "../../../service/UserService/UserService";
import { NotificationContext } from "../../../providers/notification";
import TextInput from "../../Roles/Common/TextInput/TextInput";
import { IRegister } from "../../../types/IRegister";
import { NotificationType } from "../../../types/INotification";

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    backgroundColor: "white",
    padding: "26px",
    borderRadius: "16px",
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(3),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

interface IProps {
  history: any;
}

const SignUp: React.FC<IProps> = (props: IProps) => {
  const classes = useStyles();
  const notificationContext = useContext(NotificationContext);

  const Register = async (data: IRegister) => {
    try {
      var response = await UserApi.Register(data);
      notificationContext.setSnackbar({
        showSnackbar: true,
        message: "Successfully registered!",
        type: NotificationType.Success,
      });
      setTimeout(() => {
        props.history.push("/payer/list-of-expenses");
      }, 3000);
    } catch (err) {
      console.log(err);
    }
  };

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Registracija
        </Typography>
        <Formik
          initialValues={{
            firstName: "",
            lastName: "",
            dateOfBirth: "",
            email: "",
            password: "",
            iban: "",
            oib: "",
          }}
          onSubmit={(values: any) => Register(values)}
        >
          {(props: any) => {
            const {
              touched,
              errors,
              isSubmitting,
              handleChange,
              handleBlur,
              handleSubmit,
            } = props;
            return (
              <form
                className={classes.form + " " + styles.highInput}
                onSubmit={handleSubmit}
              >
                <div className={styles.inputHolder}>
                  <TextInput
                    handleChange={handleChange}
                    handleBlur={handleBlur}
                    id="firstName"
                    errors={errors}
                    touched={touched}
                    value={props.values && props.values.firstName}
                    placeholder="Ime"
                  />
                </div>
                <div className={styles.inputHolder}>
                  <TextInput
                    handleChange={handleChange}
                    handleBlur={handleBlur}
                    id="lastName"
                    errors={errors}
                    touched={touched}
                    value={props.values && props.values.lastName}
                    placeholder="Prezime"
                  />
                </div>
                <div className={styles.inputHolder}>
                  <TextInput
                    handleChange={handleChange}
                    handleBlur={handleBlur}
                    id="dateOfBirth"
                    errors={errors}
                    touched={touched}
                    value={props.values && props.values.dateOfBirth}
                    placeholder="Datum rođenja"
                  />
                </div>
                <div className={styles.inputHolder}>
                  <TextInput
                    handleChange={handleChange}
                    handleBlur={handleBlur}
                    id="email"
                    errors={errors}
                    touched={touched}
                    value={props.values && props.values.email}
                    placeholder="Email"
                  />
                </div>
                <div className={styles.inputHolder}>
                  <TextInput
                    handleChange={handleChange}
                    handleBlur={handleBlur}
                    id="password"
                    type="password"
                    errors={errors}
                    touched={touched}
                    value={props.values && props.values.password}
                    placeholder="Password"
                  />
                </div>
                <div className={styles.inputHolder}>
                  <TextInput
                    handleChange={handleChange}
                    handleBlur={handleBlur}
                    id="iban"
                    errors={errors}
                    touched={touched}
                    value={props.values && props.values.iban}
                    placeholder="IBAN"
                  />
                </div>
                <div className={styles.inputHolder}>
                  <TextInput
                    handleChange={handleChange}
                    handleBlur={handleBlur}
                    id="oib"
                    errors={errors}
                    touched={touched}
                    value={props.values && props.values.oib}
                    placeholder="OIB"
                  />
                </div>
                <Button
                  type="submit"
                  fullWidth
                  variant="contained"
                  color="primary"
                  className={classes.submit}
                  disabled={isSubmitting}
                >
                  Registriraj se
                </Button>
                <Grid container justify="flex-end">
                  <Grid item>
                    <Link href="/" variant="body2">
                      Već imate račun? Prijavite se
                    </Link>
                  </Grid>
                </Grid>
              </form>
            );
          }}
        </Formik>
      </div>
    </Container>
  );
};

export default SignUp;
