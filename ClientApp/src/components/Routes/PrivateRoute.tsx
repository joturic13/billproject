import { Redirect, Route } from "react-router";
import React from 'react';
import NavBar from "../Roles/Common/NavBar/NavBar";
import ContentLayout from "../Roles/Common/Layout/ContentLayout";

const PrivateRoute = ({
    component: Component,
    history,
    ...rest
}: any) => (
        <NavBar history={history}>
            <ContentLayout>
                <Route {...rest}
                    render={(props: any) =><Component {...props} />
                } />
            </ContentLayout>
        </NavBar>
    );


export default PrivateRoute;