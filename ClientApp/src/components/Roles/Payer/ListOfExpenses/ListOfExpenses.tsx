import React, { useState, useEffect, useContext } from "react";
import { IGetExpense } from "../../../../types/ICreateExpense";
import CardHeader from "../../Common/Card/CardTitle";
import ComponentName from "../../Common/ComponentName/ComponentName";
import Expense from "../../Common/Expense/Expense";
import styles from "./styles.module.scss";
import ExpenseApi from "../../../../service/ExpenseService/ExpenseService";
import { NotificationContext } from "../../../../providers/notification";
import { APP } from "../../../../constants/appRoutes";
import { NotificationType } from "../../../../types/INotification";
import LinearProgress from "@material-ui/core/LinearProgress";
import {
  FormControl,
  Grid,
  InputLabel,
  makeStyles,
  MenuItem,
  Select,
} from "@material-ui/core";

interface IProps {
  history: any;
}

interface IOwnerOption {
  name: string;
  value: string;
}

const loaderUseStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
    "& > * + *": {
      marginTop: theme.spacing(2),
    },
  },
}));

const useStyles = makeStyles((theme) => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
}));

const ListOfExpensesToPay: React.FC<IProps> = (props: IProps) => {
  const classes = useStyles();
  const loaderClasses = loaderUseStyles();
  const [expenses, setExpenses] = useState<IGetExpense[]>([]);
  const notificationContext = useContext(NotificationContext);
  const [expenseOwner, setExpenseOwner] = useState("");
  const [expenseOwnerOptions, setExpenseOwnerOptions] = useState<
    IOwnerOption[]
  >([]);
  const [loading, setLoading] = useState(false);

  const CleanFilters = async () => {
    setExpenseOwner("");
    var expenses = await ExpenseApi.FilterPayerExpenses("all");
    setExpenses(expenses);
  };

  const Filter = async () => {
    var expenses = await ExpenseApi.FilterPayerExpenses(
      expenseOwner == "" ? "all" : expenseOwner
    );
    setExpenses(expenses);
  };

  async function getExpenses() {
    setLoading(true);
    var expenses = await ExpenseApi.GetExpensesToPay();
    setExpenses(expenses);
    var options: IOwnerOption[] = [];
    expenses.forEach((e) => {
      const checkUsername = (obj: any) => obj.value === e.personInNeed.id;
      if (options.some(checkUsername) == false)
        options.push({
          name: e.personInNeed.fullName,
          value: e.personInNeed.id,
        });
    });
    setExpenseOwnerOptions(options);
    setLoading(false);
  }

  const payExpense = async (id: number) => {
    await ExpenseApi.PayExpense(id);
    notificationContext.setSnackbar({
      showSnackbar: true,
      message: "Successfully payed!",
      type: NotificationType.Success,
    });
    setExpenseOwner("");
    setTimeout(async () => {
      await getExpenses();
    }, 3000);
  };

  useEffect(() => {
    getExpenses();
  }, []);

  return (
    <div className={styles.container}>
      <ComponentName name="Pregled troškova" />
      <div className={styles.innerContainer}>
        <CardHeader
          title="Svi troškovi"
          subtitle="Pregled svih troškova u sustavu"
        />
        <div className={styles.personCont}>
          <div className={styles.filterContainer}>
            <div>Filteri</div>
            <Grid item container>
              <Grid container item xs={4}>
                <FormControl
                  variant="outlined"
                  className={classes.formControl + " " + styles.formControl}
                >
                  <InputLabel
                    id="demo-simple-select-outlined-label"
                    className={styles.inputLabel}
                  >
                    Vlasnik troška
                  </InputLabel>
                  <Select
                    labelId="demo-simple-select-outlined-label"
                    id="demo-simple-select-outlined"
                    value={expenseOwner}
                    onChange={(e) => setExpenseOwner(e.target.value as string)}
                    label="Vlasnik troška"
                  >
                    {expenseOwnerOptions.map((option, i) => (
                      <MenuItem key={i} value={option.value}>
                        {option.name}
                      </MenuItem>
                    ))}
                  </Select>
                </FormControl>
              </Grid>
              <Grid item container xs={3}>
                <button
                  className={styles.cancelBtn}
                  onClick={() => CleanFilters()}
                >
                  Poništi
                </button>
                <button className={styles.filterBtn} onClick={() => Filter()}>
                  Filtriraj
                </button>
              </Grid>
            </Grid>
          </div>
          {loading && (
            <div className={loaderClasses.root + " " + styles.loadingContainer}>
              <LinearProgress />
            </div>
          )}
          {expenses.map((expense, i) => (
            <div key={i}>
              <Expense payExpense={payExpense} expense={expense} />
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};

export default ListOfExpensesToPay;
