import React, { useState, useEffect, useContext } from "react";
import { IGetExpense } from "../../../../types/ICreateExpense";
import CardHeader from "../../Common/Card/CardTitle";
import ComponentName from "../../Common/ComponentName/ComponentName";
import Expense from "../../Common/Expense/Expense";
import styles from "./styles.module.scss";
import ExpenseApi from "../../../../service/ExpenseService/ExpenseService";
import { NotificationContext } from "../../../../providers/notification";
import { APP } from "../../../../constants/appRoutes";
import { NotificationType } from "../../../../types/INotification";
import LinearProgress from "@material-ui/core/LinearProgress";
import FormatListNumberedIcon from "@material-ui/icons/FormatListNumbered";
import MoneyIcon from "@material-ui/icons/Money";
import { IStatistics } from "../../../../types/IStatistics";
import StatisticsApi from "../../../../service/StatisticsService/StatisticService";
import {
  FormControl,
  Grid,
  InputLabel,
  makeStyles,
  MenuItem,
  Select,
} from "@material-ui/core";

interface IProps {
  history: any;
}

interface IOwnerOption {
  name: string;
  value: string;
}
const loaderUseStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
    "& > * + *": {
      marginTop: theme.spacing(2),
    },
  },
}));

const ListOfPayedExpenses: React.FC<IProps> = (props: IProps) => {
  const [expenses, setExpenses] = useState<IGetExpense[]>([]);
  const loaderClasses = loaderUseStyles();
  const [loading, setLoading] = useState(false);
  const [statistics, setStatistics] = useState<IStatistics>();

  async function getExpenses() {
    setLoading(true);
    var expenses = await ExpenseApi.GetPayedExpensesd();
    setExpenses(expenses);
    setLoading(false);
  }

  async function getStatistics() {
    var statistics = await StatisticsApi.GetPayerStatistics();
    setStatistics(statistics);
    console.log(statistics);
  }

  useEffect(() => {
    getExpenses();
    getStatistics();
  }, []);

  return (
    <div className={styles.container}>
      <ComponentName name="Pregled troškova" />
      <div className={styles.innerContainer}>
        <CardHeader
          title="Plaćeni troškovi"
          subtitle="Pregled troškova koje ste platili"
        />
        <div className={styles.statisticContainer}>
          <Grid item container spacing={3}>
            <Grid item container xs={12}>
              <div className={styles.title}>Statistika o troškovima</div>
            </Grid>
            <Grid item container xs={6}>
              <FormatListNumberedIcon />
              Platili ste ukupno troškova:
              <span>{statistics?.expenseStatistics.numOfPayedExpenses}</span>
            </Grid>
            <Grid item container xs={6}>
              <MoneyIcon /> Ukupna suma troškova koje ste platili:
              <span>{statistics?.expenseStatistics.sumOfExpenses} kn</span>
            </Grid>
          </Grid>
        </div>
        <div className={styles.personCont}>
          {loading && (
            <div className={loaderClasses.root + " " + styles.loadingContainer}>
              <LinearProgress />
            </div>
          )}
          {expenses.map((expense, i) => (
            <div key={i}>
              <Expense expense={expense} />
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};

export default ListOfPayedExpenses;
