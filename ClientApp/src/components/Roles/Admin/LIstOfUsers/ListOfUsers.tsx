import React, { useEffect, useState, useContext } from "react";
import { ICreatePerson, IPerson } from "../../../../types/IPerson";
import CardHeader from "../../Common/Card/CardTitle";
import ComponentName from "../../Common/ComponentName/ComponentName";
import Person from "../../Common/Person/Person";
import styles from "./styles.module.scss";
import PersonApi from "../../../../service/PersonService/PersonService";
import { APP } from "../../../../constants/appRoutes";
import { NotificationContext } from "../../../../providers/notification";
import { NotificationType } from "../../../../types/INotification";
import LinearProgress from "@material-ui/core/LinearProgress";
import { makeStyles } from "@material-ui/core";

interface IProps {
  history: any;
}

const loaderUseStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
    "& > * + *": {
      marginTop: theme.spacing(2),
    },
  },
}));

const ListOfUsers: React.FC<IProps> = (props: IProps) => {
  const [users, setUsers] = useState<ICreatePerson[]>([]);
  const notificationContext = useContext(NotificationContext);
  const loaderClasses = loaderUseStyles();
  const [loading, setLoading] = useState(false);

  const goToEditUser = () => {
    props.history.push(APP.ROLES.ADMIN.UPDATE_USER);
  };

  async function getPeopleInNeed() {
    setLoading(true);
    var users = await PersonApi.GetPeopleInNeed();
    setUsers(users);
    setLoading(false);
  }

  useEffect(() => {
    getPeopleInNeed();
  }, []);

  const deletePersonInNeed = async (id: string) => {
    await PersonApi.DeletePersonInNeed(id);
    notificationContext.setSnackbar({
      showSnackbar: true,
      message: "Uspješno obrisano!",
      type: NotificationType.Success,
    });
    setTimeout(async () => {
      await getPeopleInNeed();
    }, 3000);
  };

  return (
    <div className={styles.container}>
      <ComponentName name="Pregled osoba" />
      <div className={styles.innerContainer}>
        <CardHeader
          title="Osobe u sustavu"
          subtitle="Pregled svih osoba unesenih u sustav"
        />
        <div className={styles.personCont}>
          {loading && (
            <div className={loaderClasses.root + " " + styles.loadingContainer}>
              <LinearProgress />
            </div>
          )}
          {users.map((person, i) => (
            <div key={i}>
              <Person
                goToEditUser={goToEditUser}
                delete={deletePersonInNeed}
                person={person}
              />
            </div>
          ))}
        </div>
        <div className={styles.buttonDiv}>
          <button
            onClick={() => props.history.push(APP.ROLES.ADMIN.DASHBOARD)}
            className={styles.addButton}
          >
            Dodaj osobu
          </button>
        </div>
      </div>
    </div>
  );
};

export default ListOfUsers;
