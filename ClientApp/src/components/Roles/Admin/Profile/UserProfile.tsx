import { Grid } from "@material-ui/core";
import { Formik } from "formik";
import React, { useContext, useState } from "react";
import * as Yup from "yup";
import { ICreatePerson } from "../../../../types/IPerson";
import CardHeader from "../../Common/Card/CardTitle";
import CheckBox from "../../Common/CheckBoxInput/CheckBox";
import PersonApi from "../../../../service/PersonService/PersonService";
import ComponentName from "../../Common/ComponentName/ComponentName";
import TextInput from "../../Common/TextInput/TextInput";
import styles from "./styles.module.scss";
import { NotificationType } from "../../../../types/INotification";
import { NotificationContext } from "../../../../providers/notification";

interface IProps {}

const UserProfile: React.FC<IProps> = (props: IProps) => {
  const notificationContext = useContext(NotificationContext);

  const createUser = async (user: ICreatePerson) => {
    try {
      var response = await PersonApi.CreatePerson(user);
      notificationContext.setSnackbar({
        showSnackbar: true,
        message: "Uspješno stvoreno!",
        type: NotificationType.Success,
      });
    } catch (err) {
      console.log(err);
    }
  };

  return (
    <div className={styles.otusideContainer}>
      <div>
        <ComponentName name="Nova osoba" />
        <div className={styles.container}>
          <CardHeader title="Korisnički profil" subtitle="Unesite novu osobu" />
          <div className={styles.form}>
            <Formik
              enableReinitialize
              initialValues={{
                id: "",
                firstName: "",
                lastName: "",
                dateOfBirth: "",
                email: "",
                password: "",
                oib: "",
                address: "",
                iban: "",
                showPersonalData: false,
                familyNote: "",
              }}
              onSubmit={(values: any, { resetForm }) => {
                console.log(values);
                createUser(values);
                resetForm({});
              }}
              validationSchema={Yup.object().shape({
                firstName: Yup.string().required("Obavezno"),
                lastName: Yup.string().required("Obavezno"),
                dateOfBirth: Yup.string().required("Obavezno"),
                email: Yup.string().required("Obavezno"),
                password: Yup.string().required("Obavezno"),
                oib: Yup.string().required("Obavezno"),
                address: Yup.string().required("Obavezno"),
                iban: Yup.string().required("Obavezno"),
              })}
            >
              {(props: any) => {
                const {
                  touched,
                  errors,
                  isSubmitting,
                  handleChange,
                  handleBlur,
                  handleSubmit,
                } = props;
                return (
                  <form onSubmit={handleSubmit}>
                    <Grid
                      container
                      item
                      xs={12}
                      className={styles.formContainer}
                      justify="center"
                      spacing={4}
                    >
                      <Grid container item xs={12} spacing={2}>
                        <Grid container item xs={3}>
                          <TextInput
                            handleChange={handleChange}
                            handleBlur={handleBlur}
                            id="firstName"
                            errors={errors}
                            touched={touched}
                            value={props.values && props.values.firstName}
                            placeholder="Ime"
                          />
                        </Grid>
                        <Grid container item xs={3}>
                          <TextInput
                            handleChange={handleChange}
                            handleBlur={handleBlur}
                            id="lastName"
                            errors={errors}
                            touched={touched}
                            value={props.values && props.values.lastName}
                            placeholder="Prezime"
                          />
                        </Grid>
                        <Grid container item xs={3}>
                          <TextInput
                            handleChange={handleChange}
                            handleBlur={handleBlur}
                            id="dateOfBirth"
                            errors={errors}
                            touched={touched}
                            value={props.values && props.values.dateOfBirth}
                            placeholder="Datum rođenja"
                          />
                        </Grid>
                      </Grid>
                      <Grid container item xs={12} spacing={2}>
                        <Grid container item xs={3}>
                          <TextInput
                            handleChange={handleChange}
                            handleBlur={handleBlur}
                            id="email"
                            errors={errors}
                            touched={touched}
                            value={props.values && props.values.email}
                            placeholder="Email"
                          />
                        </Grid>
                        <Grid container item xs={3}>
                          <TextInput
                            handleChange={handleChange}
                            handleBlur={handleBlur}
                            id="password"
                            errors={errors}
                            touched={touched}
                            value={props.values && props.values.password}
                            placeholder="Password"
                          />
                        </Grid>
                        <Grid container item xs={3}>
                          <TextInput
                            handleChange={handleChange}
                            handleBlur={handleBlur}
                            id="oib"
                            errors={errors}
                            touched={touched}
                            value={props.values && props.values.oib}
                            placeholder="OIB"
                          />
                        </Grid>
                      </Grid>
                      <Grid container item xs={12} spacing={2}>
                        <Grid container item xs={3}>
                          <TextInput
                            handleChange={handleChange}
                            handleBlur={handleBlur}
                            id="address"
                            errors={errors}
                            multiline={false}
                            touched={touched}
                            value={props.values && props.values.address}
                            placeholder="Adresa"
                          />
                        </Grid>
                        <Grid container item xs={3}>
                          <TextInput
                            handleChange={handleChange}
                            handleBlur={handleBlur}
                            id="iban"
                            errors={errors}
                            touched={touched}
                            value={props.values && props.values.iban}
                            placeholder="IBAN"
                          />
                        </Grid>
                      </Grid>
                      <Grid container item xs={12} justify="flex-end">
                        <Grid item container xs={2}>
                          <button
                            type="submit"
                            disabled={isSubmitting}
                            className={styles.addButton}
                          >
                            Dodaj
                          </button>
                        </Grid>
                      </Grid>
                    </Grid>
                  </form>
                );
              }}
            </Formik>
          </div>
        </div>
      </div>
    </div>
  );
};

export default UserProfile;
