import React, { useEffect, useState } from "react";
import CardHeader from "../../Common/Card/CardTitle";
import ComponentName from "../../Common/ComponentName/ComponentName";
import StatisticsApi from "../../../../service/StatisticsService/StatisticService";
import ShowChartIcon from "@material-ui/icons/ShowChart";
import FormatListNumberedIcon from "@material-ui/icons/FormatListNumbered";
import styles from "./styles.module.scss";
import MoneyIcon from "@material-ui/icons/Money";
import { IStatistics } from "../../../../types/IStatistics";
import { Divider, Grid } from "@material-ui/core";

interface IProps {}

const AdminDashboard: React.FC<IProps> = (props: IProps) => {
  const [statistics, setStatistics] = useState<IStatistics>();

  async function getStatistics() {
    var statistics = await StatisticsApi.GetStatistics();
    console.log(statistics);
    setStatistics(statistics);
  }

  useEffect(() => {
    getStatistics();
  }, []);

  return (
    <div className={styles.outsideContainer}>
      <div className={styles.container}>
        <ComponentName name="Admin dashbooard" />
        <div className={styles.innerContainer}>
          <CardHeader title="Dashboard" subtitle="Pregled statistike" />
          <div className={styles.statisticContainer}>
            <Grid item container spacing={3}>
              <Grid item container xs={12}>
                <div className={styles.title}>Statistika o troškovima</div>
              </Grid>
              <Grid item container xs={6}>
                <FormatListNumberedIcon />
                Ukupno troškova u sustavu
                <span>{statistics?.expenseStatistics.totalNumOfExpenses}</span>
              </Grid>
              <Grid item container xs={6}>
                <MoneyIcon /> Ukupna suma troškova u sustavu
                <span>{statistics?.expenseStatistics.sumOfExpenses} kn</span>
              </Grid>
              <Grid item container xs={6}>
                <FormatListNumberedIcon /> Broj plaćenih troškova
                <span>{statistics?.expenseStatistics.numOfPayedExpenses}</span>
              </Grid>
              <Grid item container xs={6}>
                <MoneyIcon /> Suma plaćenih troškova
                <span>
                  {statistics?.expenseStatistics.sumOfPayedExpenses} kn
                </span>
              </Grid>
              <Grid item container xs={6}>
                <ShowChartIcon /> Postotak plaćenih troškova
                <span>
                  {statistics?.expenseStatistics.percentageOfPayedExpenses! *
                    100}
                  %
                </span>
              </Grid>
            </Grid>
            <Divider className={styles.divider} />
            <Grid item container spacing={3}>
              <Grid item container xs={12}>
                <div className={styles.title}>
                  Statistika o ljudima u potrebi
                </div>
              </Grid>
              <Grid item container xs={6}>
                <FormatListNumberedIcon />
                Ukupno ljudi u potrebi u sustavu
                <span>
                  {statistics?.peopleInNeedStatistics.numOfPeopleInNeed}
                </span>
              </Grid>
            </Grid>
            <Divider className={styles.divider} />
            <Grid item container spacing={3}>
              <Grid item container xs={12}>
                <div className={styles.title}>Statistika o korisnicima</div>
              </Grid>
              <Grid item container xs={6}>
                <FormatListNumberedIcon />
                Ukupno korisnika u stustavu
                <span>{statistics?.payerStatistics.numOfPayers}</span>
              </Grid>
            </Grid>
          </div>
        </div>
      </div>
    </div>
  );
};

export default AdminDashboard;
