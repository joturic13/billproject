import { Grid } from "@material-ui/core";
import { Formik } from "formik";
import React, { useContext, useState } from "react";
import * as Yup from "yup";
import { ICreatePerson } from "../../../../types/IPerson";
import CardHeader from "../../Common/Card/CardTitle";
import PersonApi from "../../../../service/PersonService/PersonService";
import ComponentName from "../../Common/ComponentName/ComponentName";
import TextInput from "../../Common/TextInput/TextInput";
import styles from "./styles.module.scss";
import { NotificationType } from "../../../../types/INotification";
import { NotificationContext } from "../../../../providers/notification";
import { UserContext } from "../../../../providers/user";
import { APP } from "../../../../constants/appRoutes";

interface IProps {
  history: any;
}

const UpdateProfile: React.FC<IProps> = (props: IProps) => {
  const notificationContext = useContext(NotificationContext);
  const userContext = useContext(UserContext);
  const { user } = userContext;

  const updateUser = async (user: ICreatePerson) => {
    console.log(user);
    try {
      var response = await PersonApi.UpdatePerson(user);
      notificationContext.setSnackbar({
        showSnackbar: true,
        message: "User updated!",
        type: NotificationType.Success,
      });
      setTimeout(() => {
        props.history.push(APP.ROLES.ADMIN.LIST_OF_USERS);
      }, 3500);
    } catch (err) {
      console.log(err);
    }
  };

  return (
    <div className={styles.otusideContainer}>
      <div>
        <ComponentName name="Nova osoba" />
        <div className={styles.container}>
          <CardHeader title="Korisnički profil" subtitle="Unesite novu osobu" />
          <div className={styles.form}>
            <Formik
              enableReinitialize={true}
              initialValues={{
                id: userContext.user.id,
                firstName: userContext.user.firstName,
                lastName: userContext.user.lastName,
                dateOfBirth: userContext.user.dateOfBirth,
                email: userContext.user.email,
                password: userContext.user.password,
                oib: userContext.user.oib,
                address: userContext.user.address,
                iban: userContext.user.iban,
                showPersonalData: userContext.user.showPersonalData,
                familyNote: userContext.user.familyNote,
              }}
              onSubmit={(values: any) => {
                console.log("Updateeee");
                updateUser(values);
              }}
              validationSchema={Yup.object().shape({
                firstName: Yup.string().required("Obavezno"),
                lastName: Yup.string().required("Obavezno"),
                dateOfBirth: Yup.string().required("Obavezno"),
                email: Yup.string().required("Obavezno"),
                password: Yup.string().required("Obavezno"),
                oib: Yup.string().required("Obavezno"),
                address: Yup.string().required("Obavezno"),
                iban: Yup.string().required("Obavezno"),
                familyNote: Yup.string().required("Obavezno"),
              })}
            >
              {(props: any) => {
                const {
                  touched,
                  errors,
                  isSubmitting,
                  handleChange,
                  handleBlur,
                  handleSubmit,
                } = props;
                return (
                  <form onSubmit={handleSubmit}>
                    <Grid
                      container
                      item
                      xs={12}
                      className={styles.formContainer}
                      justify="center"
                      spacing={4}
                    >
                      <Grid container item xs={12} spacing={2}>
                        <Grid container item xs={3}>
                          <TextInput
                            handleChange={(e) =>
                              userContext.setUser({
                                ...user,
                                firstName: e.target.value,
                              })
                            }
                            handleBlur={handleBlur}
                            id="firstName"
                            errors={errors}
                            touched={touched}
                            value={props.values && props.values.firstName}
                            placeholder="Ime"
                          />
                        </Grid>
                        <Grid container item xs={3}>
                          <TextInput
                            handleChange={(e) =>
                              userContext.setUser({
                                ...user,
                                lastName: e.target.value,
                              })
                            }
                            handleBlur={handleBlur}
                            id="lastName"
                            errors={errors}
                            touched={touched}
                            value={props.values && props.values.lastName}
                            placeholder="Prezime"
                          />
                        </Grid>
                        <Grid container item xs={3}>
                          <TextInput
                            handleChange={(e) =>
                              userContext.setUser({
                                ...user,
                                dateOfBirth: e.target.value,
                              })
                            }
                            handleBlur={handleBlur}
                            id="dateOfBirth"
                            errors={errors}
                            touched={touched}
                            value={props.values && props.values.dateOfBirth}
                            placeholder="Datum rođenja"
                          />
                        </Grid>
                      </Grid>
                      <Grid container item xs={12} spacing={2}>
                        <Grid container item xs={3}>
                          <TextInput
                            handleChange={(e) =>
                              userContext.setUser({
                                ...user,
                                email: e.target.value,
                              })
                            }
                            handleBlur={handleBlur}
                            id="email"
                            errors={errors}
                            touched={touched}
                            value={props.values && props.values.email}
                            placeholder="Email"
                          />
                        </Grid>
                        <Grid container item xs={3}>
                          <TextInput
                            handleChange={(e) =>
                              userContext.setUser({
                                ...user,
                                oib: e.target.value,
                              })
                            }
                            handleBlur={handleBlur}
                            id="oib"
                            errors={errors}
                            touched={touched}
                            value={props.values && props.values.oib}
                            placeholder="OIB"
                          />
                        </Grid>
                      </Grid>
                      <Grid container item xs={12} spacing={2}>
                        <Grid container item xs={3}>
                          <TextInput
                            handleChange={(e) =>
                              userContext.setUser({
                                ...user,
                                address: e.target.value,
                              })
                            }
                            handleBlur={handleBlur}
                            id="address"
                            errors={errors}
                            multiline={false}
                            touched={touched}
                            value={props.values && props.values.address}
                            placeholder="Adresa"
                          />
                        </Grid>
                        <Grid container item xs={3}>
                          <TextInput
                            handleChange={(e) =>
                              userContext.setUser({
                                ...user,
                                iban: e.target.value,
                              })
                            }
                            handleBlur={handleBlur}
                            id="iban"
                            errors={errors}
                            touched={touched}
                            value={props.values && props.values.iban}
                            placeholder="IBAN"
                          />
                        </Grid>
                      </Grid>
                      <Grid container item xs={12} justify="flex-end">
                        <Grid item container xs={2}>
                          <button
                            type="submit"
                            onClick={() => updateUser(user)}
                            disabled={isSubmitting}
                            className={styles.addButton}
                          >
                            Dodaj
                          </button>
                        </Grid>
                      </Grid>
                    </Grid>
                  </form>
                );
              }}
            </Formik>
          </div>
        </div>
      </div>
    </div>
  );
};

export default UpdateProfile;
