import {
  FormControl,
  Grid,
  InputLabel,
  MenuItem,
  Select,
} from "@material-ui/core";
import { Formik } from "formik";
import { makeStyles } from "@material-ui/core/styles";
import React, { useState, useEffect, useContext } from "react";
import * as Yup from "yup";
import CardHeader from "../../Common/Card/CardTitle";
import ComponentName from "../../Common/ComponentName/ComponentName";
import TextInput from "../../Common/TextInput/TextInput";
import styles from "./styles.module.scss";
import { ICreatePerson, IPerson } from "../../../../types/IPerson";
import PersonApi from "../../../../service/PersonService/PersonService";
import ExpenseApi from "../../../../service/ExpenseService/ExpenseService";
import { IExpenseTypes } from "../../../../types/IExpenseTypes";
import { ExpenseEnum } from "../../../../constants/enums/expenseEnum";
import { ICreateExpense } from "../../../../types/ICreateExpense";
import { NotificationContext } from "../../../../providers/notification";
import { NotificationType } from "../../../../types/INotification";

interface IProps {}
const useStyles = makeStyles((theme) => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
}));
const NewExpense: React.FC<IProps> = (props: IProps) => {
  const classes = useStyles();
  const [expenseOwners, setExpenseOwners] = useState<ICreatePerson[]>([]);
  const [expenseTypes, setExpenseTypes] = useState<IExpenseTypes[]>([]);
  const notificationContext = useContext(NotificationContext);

  const createExpense = async (expense: ICreateExpense) => {
    try {
      var response = await ExpenseApi.CreateExpense(expense);
      notificationContext.setSnackbar({
        showSnackbar: true,
        message: "Expense created!",
        type: NotificationType.Success,
      });
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    async function getPeopleInNeed() {
      var users = await PersonApi.GetPeopleInNeed();
      setExpenseOwners(users);
    }
    getPeopleInNeed();
    setExpenseTypes(ExpenseEnum);
  }, []);

  return (
    <div className={styles.outerContainer}>
      <ComponentName name="Novi trošak" />
      <div className={styles.container}>
        <CardHeader title="Trošak" subtitle="Unesite novi trošak" />
        <div className={styles.form}>
          <Formik
            initialValues={{
              id: "",
              personInNeedId: "",
              payerId: "",
              expenseType: "",
              expenseDescription: "",
              expenseAmount: "",
            }}
            onSubmit={(values: any, { resetForm }) => {
              createExpense(values);
              resetForm({});
            }}
            validationSchema={Yup.object().shape({
              personInNeedId: Yup.string().required("Obavezno"),
              expenseType: Yup.string().required("Obavezno"),
              expenseDescription: Yup.string().required("Obavezno"),
              expenseAmount: Yup.string().required("Obavezno"),
            })}
          >
            {(props: any) => {
              const {
                touched,
                errors,
                isSubmitting,
                handleChange,
                setFieldValue,
                handleBlur,
                handleSubmit,
              } = props;
              return (
                <form onSubmit={handleSubmit}>
                  <Grid
                    container
                    item
                    xs={12}
                    className={styles.formContainer}
                    justify="center"
                    spacing={4}
                  >
                    <Grid container item xs={12} spacing={2}>
                      <Grid container item xs={3}>
                        <FormControl
                          variant="outlined"
                          className={
                            classes.formControl + " " + styles.formControl
                          }
                        >
                          <InputLabel
                            id="demo-simple-select-outlined-label"
                            className={styles.inputLabel}
                          >
                            Vlasnik troška
                          </InputLabel>
                          <Select
                            labelId="demo-simple-select-outlined-label"
                            id="demo-simple-select-outlined"
                            value={props.values && props.values.personInNeedId}
                            onChange={(e) =>
                              setFieldValue("personInNeedId", e.target.value)
                            }
                            label="Vlasnik troška"
                          >
                            {expenseOwners.map((person, i) => (
                              <MenuItem key={i} value={person.id}>
                                {person.fullName}
                              </MenuItem>
                            ))}
                          </Select>
                        </FormControl>
                      </Grid>
                      <Grid container item xs={3}>
                        <FormControl
                          variant="outlined"
                          className={
                            classes.formControl + " " + styles.formControl
                          }
                        >
                          <InputLabel
                            id="demo-simple-select-outlined-label"
                            className={styles.inputLabel}
                          >
                            Vrste troška
                          </InputLabel>
                          <Select
                            labelId="demo-simple-select-outlined-label"
                            id="demo-simple-select-outlined"
                            value={props.values && props.values.expenseType}
                            onChange={(e) =>
                              setFieldValue("expenseType", e.target.value)
                            }
                            label="Expense type"
                          >
                            {expenseTypes.map((expense, i) => (
                              <MenuItem key={i} value={expense.value}>
                                {expense.name}
                              </MenuItem>
                            ))}
                          </Select>
                        </FormControl>
                      </Grid>
                      <Grid container item xs={3}>
                        <TextInput
                          handleChange={handleChange}
                          handleBlur={handleBlur}
                          id="expenseAmount"
                          errors={errors}
                          type="number"
                          touched={touched}
                          value={props.values && props.values.expenseAmount}
                          placeholder="Iznos (kn)"
                        />
                      </Grid>
                    </Grid>
                    <Grid container item xs={12} spacing={2}>
                      <Grid container item xs={6}>
                        <TextInput
                          handleChange={handleChange}
                          handleBlur={handleBlur}
                          id="expenseDescription"
                          multiline={true}
                          errors={errors}
                          touched={touched}
                          value={
                            props.values && props.values.expenseDescription
                          }
                          placeholder="Opis troška"
                        />
                      </Grid>
                    </Grid>
                    <Grid container item xs={12} justify="flex-end">
                      <Grid item container xs={2}>
                        <button
                          type="submit"
                          disabled={isSubmitting}
                          className={styles.addButton}
                        >
                          Dodaj
                        </button>
                      </Grid>
                    </Grid>
                  </Grid>
                </form>
              );
            }}
          </Formik>
        </div>
      </div>
    </div>
  );
};

export default NewExpense;
