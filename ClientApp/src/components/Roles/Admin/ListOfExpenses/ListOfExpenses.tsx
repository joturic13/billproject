import React, { useState, useEffect, useContext } from "react";
import { IGetExpense } from "../../../../types/ICreateExpense";
import CardHeader from "../../Common/Card/CardTitle";
import ComponentName from "../../Common/ComponentName/ComponentName";
import Expense from "../../Common/Expense/Expense";
import styles from "./styles.module.scss";
import ExpenseApi from "../../../../service/ExpenseService/ExpenseService";
import { APP } from "../../../../constants/appRoutes";
import { NotificationContext } from "../../../../providers/notification";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import { NotificationType } from "../../../../types/INotification";
import Checkbox from "@material-ui/core/Checkbox";
import LinearProgress from "@material-ui/core/LinearProgress";
import {
  FormControl,
  Grid,
  InputLabel,
  makeStyles,
  MenuItem,
  Select,
} from "@material-ui/core";

interface IProps {
  history: any;
}

const loaderUseStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
    "& > * + *": {
      marginTop: theme.spacing(2),
    },
  },
}));

interface IOwnerOption {
  name: string;
  value: string;
}

const useStyles = makeStyles((theme) => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
}));

const ListOfExpenses: React.FC<IProps> = (props: IProps) => {
  const classes = useStyles();
  const loaderClasses = loaderUseStyles();
  const [expenses, setExpenses] = useState<IGetExpense[]>([]);
  const notificationContext = useContext(NotificationContext);
  const [payedFIlter, setPayedFilter] = useState("");
  const [expenseOwner, setExpenseOwner] = useState("");
  const [expenseOwnerOptions, setExpenseOwnerOptions] = useState<
    IOwnerOption[]
  >([]);
  const [loading, setLoading] = useState(false);

  async function getExpenses() {
    setLoading(true);
    var expenses = await ExpenseApi.GetAllExpenses();
    setExpenses(expenses);
    var options: IOwnerOption[] = [];
    expenses.forEach((e) => {
      const checkUsername = (obj: any) => obj.value === e.personInNeed.id;
      if (options.some(checkUsername) == false)
        options.push({
          name: e.personInNeed.fullName,
          value: e.personInNeed.id,
        });
    });
    setExpenseOwnerOptions(options);
    setLoading(false);
  }

  const CleanFilters = async () => {
    setExpenseOwner("");
    setPayedFilter("");
    var expenses = await ExpenseApi.FilterAdminExpenses("all", "all");
    setExpenses(expenses);
  };

  const goToEditExpense = () => {
    props.history.push(APP.ROLES.ADMIN.UPDATE_EXPENSE);
  };

  const Filter = async () => {
    var expenses = await ExpenseApi.FilterAdminExpenses(
      payedFIlter == "" ? "all" : payedFIlter,
      expenseOwner == "" ? "all" : expenseOwner
    );
    setExpenses(expenses);
  };

  const payedFilterOptions = [
    { name: "Plaćeno", value: "1" },
    { name: "Neplaćeno", value: "2" },
  ];

  useEffect(() => {
    getExpenses();
  }, []);

  const deleteExpense = async (id: number) => {
    await ExpenseApi.DeleteExpense(id);
    notificationContext.setSnackbar({
      showSnackbar: true,
      message: "Uspješno obrisano!",
      type: NotificationType.Success,
    });
    setTimeout(async () => {
      await getExpenses();
    }, 3000);
  };

  return (
    <div className={styles.container}>
      <ComponentName name="Pregled troškova" />
      <div className={styles.innerContainer}>
        <CardHeader
          title="Svi troškovi"
          subtitle="Pregled svih troškova u sustavu"
        />
        <div className={styles.personCont}>
          <div className={styles.filterContainer}>
            <div>Filteri</div>
            <Grid item container>
              <Grid item container xs={7} sm={9}>
                <Grid container item xs={3}>
                  <FormControl
                    variant="outlined"
                    className={classes.formControl + " " + styles.formControl}
                  >
                    <InputLabel
                      id="demo-simple-select-outlined-label"
                      className={styles.inputLabel}
                    >
                      Plaćeno
                    </InputLabel>
                    <Select
                      labelId="demo-simple-select-outlined-label"
                      id="demo-simple-select-outlined"
                      value={payedFIlter}
                      onChange={(e) => setPayedFilter(e.target.value as string)}
                      label="Plaćeno"
                    >
                      {payedFilterOptions.map((option, i) => (
                        <MenuItem key={i} value={option.value}>
                          {option.name}
                        </MenuItem>
                      ))}
                    </Select>
                  </FormControl>
                </Grid>
                <Grid container item xs={4} sm={3}>
                  <FormControl
                    variant="outlined"
                    className={classes.formControl + " " + styles.formControl}
                  >
                    <InputLabel
                      id="demo-simple-select-outlined-label"
                      className={styles.inputLabel}
                    >
                      Vlasnik troška
                    </InputLabel>
                    <Select
                      labelId="demo-simple-select-outlined-label"
                      id="demo-simple-select-outlined"
                      value={expenseOwner}
                      onChange={(e) =>
                        setExpenseOwner(e.target.value as string)
                      }
                      label="Vlasnik troška"
                    >
                      {expenseOwnerOptions.map((option, i) => (
                        <MenuItem key={i} value={option.value}>
                          {option.name}
                        </MenuItem>
                      ))}
                    </Select>
                  </FormControl>
                </Grid>
              </Grid>
              <Grid item container xs={4} sm={3}>
                <button
                  className={styles.cancelBtn}
                  onClick={() => CleanFilters()}
                >
                  Poništi
                </button>
                <button className={styles.filterBtn} onClick={() => Filter()}>
                  Filtriraj
                </button>
              </Grid>
            </Grid>
          </div>
          {loading && (
            <div className={loaderClasses.root + " " + styles.loadingContainer}>
              <LinearProgress />
            </div>
          )}
          {expenses.map((expense, i) => (
            <div key={i}>
              <Expense
                goToEditExpense={goToEditExpense}
                deleteExpense={deleteExpense}
                expense={expense}
              />
            </div>
          ))}
        </div>
        <div className={styles.buttonDiv}>
          <button
            onClick={() => props.history.push(APP.ROLES.ADMIN.NEW_EXPENSE)}
            className={styles.addButton}
          >
            Dodaj trošak
          </button>
        </div>
      </div>
    </div>
  );
};

export default ListOfExpenses;
