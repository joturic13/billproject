import React, { useState, useEffect } from "react";
import { IGetExpense } from "../../../../types/ICreateExpense";
import CardHeader from "../../Common/Card/CardTitle";
import ComponentName from "../../Common/ComponentName/ComponentName";
import Expense from "../../Common/Expense/Expense";
import styles from "./styles.module.scss";
import ExpenseApi from "../../../../service/ExpenseService/ExpenseService";
import { APP } from "../../../../constants/appRoutes";
import {
  FormControl,
  Grid,
  InputLabel,
  LinearProgress,
  makeStyles,
  MenuItem,
  Select,
} from "@material-ui/core";

interface IProps {
  history: any;
}

const loaderUseStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
    "& > * + *": {
      marginTop: theme.spacing(2),
    },
  },
}));

const useStyles = makeStyles((theme) => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
}));

const ListOfPersonalExpenses: React.FC<IProps> = (props: IProps) => {
  const classes = useStyles();
  const loaderClasses = loaderUseStyles();
  const [expenses, setExpenses] = useState<IGetExpense[]>([]);
  const [payedFIlter, setPayedFilter] = useState("");
  const [loading, setLoading] = useState(false);

  async function getExpenses() {
    setLoading(true);
    var expenses = await ExpenseApi.GetPersonalExpenses();
    setExpenses(expenses);
    setLoading(false);
  }

  const CleanFilters = async () => {
    setPayedFilter("");
    var expenses = await ExpenseApi.FilterPersonInNeedExpenses("all");
    setExpenses(expenses);
  };

  const Filter = async () => {
    var expenses = await ExpenseApi.FilterPersonInNeedExpenses(
      payedFIlter == "" ? "all" : payedFIlter
    );
    setExpenses(expenses);
  };

  const payedFilterOptions = [
    { name: "Plaćeno", value: "1" },
    { name: "Neplaćeno", value: "2" },
  ];

  useEffect(() => {
    getExpenses();
  }, []);

  const deleteExpense = async (id: number) => {
    await ExpenseApi.DeleteExpense(id);
    await getExpenses();
  };

  return (
    <div className={styles.container}>
      <ComponentName name="Pregled troškova" />
      <div className={styles.innerContainer}>
        <CardHeader
          title="Vaši troškovi"
          subtitle="Pregled vaših troškova u sustavu"
        />
        <div className={styles.personCont}>
          <div className={styles.filterContainer}>
            <div>Filteri</div>
            <Grid item container>
              <Grid item container xs={9}>
                <Grid container item xs={3}>
                  <FormControl
                    variant="outlined"
                    className={classes.formControl + " " + styles.formControl}
                  >
                    <InputLabel
                      id="demo-simple-select-outlined-label"
                      className={styles.inputLabel}
                    >
                      Plaćeno
                    </InputLabel>
                    <Select
                      labelId="demo-simple-select-outlined-label"
                      id="demo-simple-select-outlined"
                      value={payedFIlter}
                      onChange={(e) => setPayedFilter(e.target.value as string)}
                      label="Plaćeno"
                    >
                      {payedFilterOptions.map((option, i) => (
                        <MenuItem key={i} value={option.value}>
                          {option.name}
                        </MenuItem>
                      ))}
                    </Select>
                  </FormControl>
                </Grid>
              </Grid>
              <Grid item container xs={3}>
                <button
                  className={styles.cancelBtn}
                  onClick={() => CleanFilters()}
                >
                  Poništi
                </button>
                <button className={styles.filterBtn} onClick={() => Filter()}>
                  Filtriraj
                </button>
              </Grid>
            </Grid>
          </div>
          {loading && (
            <div className={loaderClasses.root + " " + styles.loadingContainer}>
              <LinearProgress />
            </div>
          )}
          {expenses.map((expense, i) => (
            <div key={i}>
              <Expense deleteExpense={deleteExpense} expense={expense} />
            </div>
          ))}
        </div>
        <div className={styles.buttonDiv}>
          <button
            onClick={() => props.history.push(APP.ROLES.ADMIN.NEW_EXPENSE)}
            className={styles.addButton}
          >
            Dodaj trošak
          </button>
        </div>
      </div>
    </div>
  );
};

export default ListOfPersonalExpenses;
