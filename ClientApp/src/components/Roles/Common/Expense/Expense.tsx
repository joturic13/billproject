import { Divider, Grid } from "@material-ui/core";
import React, { useContext } from "react";
import { IGetExpense } from "../../../../types/ICreateExpense";
import DisplayData from "../DisplayData/DisplayData";
import styles from "./styles.module.scss";
import { isPersonInNeed, isPayer } from "../../../../helpers/AccountHelper";
import { getValueFromEnum } from "../../../../helpers/EnumHelper";
import { loadStripe } from "@stripe/stripe-js";
import { ExpenseContext } from "../../../../providers/expense";

const stripePromise = loadStripe(
  "pk_test_51I5SKIJPXEookTQhQ0iVXtvqkM2QArBPOPMclLjZFIoduBxtRmvJTfRiT605tAOCWb8wUlWSyYjQRJMHqSulw33h00FIsmeRdn"
);

interface IProps {
  expense: IGetExpense;
  payExpense?: (id: number) => void;
  deleteExpense?: (id: number) => void;
  goToEditExpense?: () => void;
}

const Expense: React.FC<IProps> = (props: IProps) => {
  const { expense } = props;
  const expenseContext = useContext(ExpenseContext);

  const editExpense = () => {
    console.log(expenseContext.expense);
    expenseContext.setExpense(props.expense);
    console.log(props.expense);
    console.log(expenseContext.expense);
    props.goToEditExpense!();
  };

  return (
    <div className={styles.smallContainer}>
      <Grid
        container
        item
        xs={12}
        justify="space-around"
        className={styles.infoHolder}
      >
        <Grid container item xs={12} spacing={5} className={styles.marginTop}>
          {!isPersonInNeed() && (
            <>
              <Grid item xs={3}>
                <DisplayData
                  id="Vlasnik troška"
                  value={expense.personInNeed.fullName}
                />
              </Grid>
              <Divider orientation="vertical" className={styles.divider} />
            </>
          )}
          <Grid item xs={3}>
            <DisplayData id="Opis troška" value={expense.expenseDescription} />
          </Grid>
          <Divider orientation="vertical" className={styles.divider} />
          <Grid item xs={3}>
            <DisplayData
              id="Vrsta troška"
              value={getValueFromEnum(expense.expenseType)}
            />
          </Grid>
          <Divider orientation="vertical" className={styles.divider} />
          <Grid item xs={2}>
            <DisplayData
              id="Iznos troška"
              value={expense.expenseAmount.toLocaleString() + " kn"}
            />
          </Grid>
        </Grid>
        {expense.payed && !isPayer() && (
          <Grid container item xs={12} spacing={5} className={styles.marginTop}>
            <Grid item xs={3}>
              <DisplayData id="Platio" value={expense.payer.fullName} />
            </Grid>
            <Divider orientation="vertical" className={styles.divider} />
            <Grid item xs={4}>
              <DisplayData id="Datum plaćanja" value={expense.dateOfPayment} />
            </Grid>
          </Grid>
        )}
        {!expense.payed ? (
          <>
            {!isPayer() ? (
              <Grid
                container
                item
                xs={12}
                justify="flex-end"
                className={styles.btnContainer}
              >
                <button
                  className={styles.editBtn}
                  onClick={() => editExpense()}
                >
                  Edit
                </button>
                <button
                  onClick={() => props.deleteExpense!(expense.id)}
                  className={styles.deleteBtn}
                >
                  Delete
                </button>
              </Grid>
            ) : (
              <Grid
                container
                item
                xs={12}
                justify="flex-end"
                className={styles.btnContainer}
              >
                <script
                  src="https://js.stripe.com/v3/checkout.js"
                  className="stripe-button"
                  data-key="pk_test_51I5SKIJPXEookTQhQ0iVXtvqkM2QArBPOPMclLjZFIoduBxtRmvJTfRiT605tAOCWb8wUlWSyYjQRJMHqSulw33h00FIsmeRdn"
                  data-locale="auto"
                  data-description="Sample Charge"
                  onClick={(e) => console.log(e)}
                ></script>
                <button
                  onClick={() => props.payExpense!(expense.id)}
                  className={styles.payButton}
                >
                  Plati
                </button>
              </Grid>
            )}
          </>
        ) : null}
      </Grid>
    </div>
  );
};

export default Expense;
