import { Divider, Grid } from "@material-ui/core";
import React, { useContext } from "react";
import { UserContext } from "../../../../providers/user";
import { ICreatePerson } from "../../../../types/IPerson";
import DisplayData from "../DisplayData/DisplayData";
import styles from "./styles.module.scss";

interface IProps {
  person: ICreatePerson;
  delete?: (id: string) => void;
  goToEditUser?: () => void;
}

const Person: React.FC<IProps> = (props: IProps) => {
  var { person } = props;
  const userContext = useContext(UserContext);

  const setUser = () => {
    userContext.setUser(person);
    props.goToEditUser!();
  };

  return (
    <div className={styles.smallContainer}>
      <Grid container item xs={12} className={styles.infoHolder} spacing={3}>
        <Grid container item xs={12} spacing={5}>
          <Grid item xs={2}>
            <DisplayData
              id="Ime"
              value={person.firstName + " " + person.lastName}
            />
          </Grid>
          <Divider
            orientation="vertical"
            className={styles.divider + " " + styles.something}
          />
          <Grid item xs={3}>
            <DisplayData id="Email" value={person.email} />
          </Grid>
          <Divider orientation="vertical" className={styles.divider} />
          <Grid item xs={3}>
            <DisplayData id="Datum rođenja" value={person.dateOfBirth} />
          </Grid>
          <Divider orientation="vertical" className={styles.divider} />
          <Grid item xs={3}>
            <DisplayData id="IBAN" value={person.iban} />
          </Grid>
        </Grid>
        <Grid container item xs={12} spacing={5}>
          <Grid item xs={3}>
            <DisplayData id="Adresa" value={person.address} />
          </Grid>
        </Grid>
        <Grid
          container
          item
          xs={12}
          justify="flex-end"
          className={styles.btnContainer}
        >
          <button className={styles.editBtn} onClick={() => setUser()}>
            Edit
          </button>
          <button
            onClick={() => props.delete!(person.id)}
            className={styles.deleteBtn}
          >
            Delete
          </button>
        </Grid>
      </Grid>
    </div>
  );
};

export default Person;
