import { Checkbox, FormControlLabel } from "@material-ui/core";
import React, { ChangeEvent } from "react";
import styles from "./styles.module.scss";

interface IProps {
  handleChange?: (e: ChangeEvent<HTMLInputElement>) => void;
  handleBlur?: (e: ChangeEvent<HTMLInputElement>) => void;
  value?: boolean;
  errors?: any;
  id: string;
  touched?: any;
  label?: string;
  placeholder?: string;
  type?: string;
  disabled?: boolean;
  step?: number;
}

const CheckBox: React.FC<IProps> = (props: IProps) => {
  console.log(props.value);
  return (
    <>
      <FormControlLabel
        value="start"
        className={styles.checkbox}
        style={{ fontSize: 16 }}
        control={
          <Checkbox
            id={props.id}
            placeholder={props.placeholder}
            checked={props.value}
            disabled={props.disabled}
            onChange={props.handleChange}
            color="primary"
            inputProps={{ "aria-label": "secondary checkbox" }}
          />
        }
        label={<span className={styles.label}>Prikaži osobne podatke</span>}
        labelPlacement="start"
      />
    </>
  );
};

export default CheckBox;
