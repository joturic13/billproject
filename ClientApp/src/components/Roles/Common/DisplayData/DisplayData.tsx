import React from "react";
import styles from "./styles.module.scss";

interface IProps {
  id?: string;
  value?: string;
}

const DisplayData: React.FC<IProps> = (props: IProps) => {
  return (
    <div>
      <div className={styles.id}>{props.id}:</div>
      <div className={styles.value}>{props.value}</div>
    </div>
  );
};

export default DisplayData;
