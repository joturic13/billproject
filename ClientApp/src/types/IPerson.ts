export interface IPerson {
    id: number,
    firstName: string,
    lastName: string,
    address: string,
    showPersonalData: boolean,
    IBAN: string,
    familyNote: string,
    dateOfBirth: string
}

export interface ICreatePerson {
    id: string,
    firstName: string,
    lastName: string,
    dateOfBirth: string,
    email: string,
    password: string,
    oib: string,
    address: string,
    iban: string,
    showPersonalData: boolean,
    familyNote: string,
    fullName: string
}

export interface IUser {
    user: ICreatePerson,
    setUser: Function
}