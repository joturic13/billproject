export interface LoginResponse {
    token: string,
    username: string,
    id: string,
    role: string
}
