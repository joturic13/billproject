export interface IExpenseTypes {
    value: string,
    name: string
}