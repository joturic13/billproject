import { ICreatePerson, IPerson } from './IPerson';
export interface ICreateExpense {
    personInNeedId: string,
    payerId: string
    expenseType: number,
    expenseDescription: string,
    expenseAmount: number
}

export interface IGetExpense {
    id: number
    personInNeed: ICreatePerson,
    personInNeedId?: string,
    payer: ICreatePerson,
    dateOfPayment: string,
    expenseType: string,
    expenseDescription: string,
    expenseAmount: number
    payed: boolean
}

export interface IExpense {
    expense: IGetExpense,
    setExpense: Function
}