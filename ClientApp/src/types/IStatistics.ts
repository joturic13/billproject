export interface IStatistics {
    expenseStatistics: ExpenseStatistics,
    payerStatistics: PayersStatistics,
    peopleInNeedStatistics: PeopleInNeedStatistics
}

interface ExpenseStatistics {
    totalNumOfExpenses: number,
    sumOfExpenses: number,    
    numOfPayedExpenses: number,   
    sumOfPayedExpenses: number,   
    percentageOfPayedExpenses: number
}

interface PayersStatistics {
    numOfPayers: number
}

interface PeopleInNeedStatistics {
    numOfPeopleInNeed: number
}