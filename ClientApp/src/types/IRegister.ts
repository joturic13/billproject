export interface IRegister {
    email: string,
    password: string,
    dateOfBirth: string,
    firstName: string,
    lastName: string,
    iban: string,
    oib: string
}