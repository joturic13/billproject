import { API } from "../../constants/apiRoutes";
import { ILogin } from "../../types/ILogin";
import { IRegister } from "../../types/IRegister";
import { api } from "../api";

export default {
  Login(loginData: ILogin): Promise<any> {
    return api.post(API.USER.LOGIN, loginData);
  },
  Register(registerData: IRegister): Promise<any> {
    return api.post(API.PAYER.REGISTER, registerData);
  },
};
