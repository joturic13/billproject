import { API } from "../../constants/apiRoutes";
import { ICreatePerson } from "../../types/IPerson";
import { api } from "../api";

export default {
  CreatePerson(personData: ICreatePerson): Promise<any> {
    return api.post(API.ADMIN.CREATE_PERSON, personData);
  },
  GetPeopleInNeed(): Promise<ICreatePerson[]> {
    return api.get(API.ADMIN.GET_POPLE_IN_NEED);
  },
  DeletePersonInNeed(id: string): Promise<any> {
    console.log(id);
    return api.delete(API.ADMIN.DELETE_PERSON_IN_NEED(id));
  },
  UpdatePerson(personData: ICreatePerson): Promise<any> {
    return api.post(API.ADMIN.UPDATE_PERSON(personData.id), personData);
  },
};
