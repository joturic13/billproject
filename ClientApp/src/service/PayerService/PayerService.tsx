import { API } from "../../constants/apiRoutes";
import { IRegister } from "../../types/IRegister";
import { api } from "../api";

export default {
  Register(registerData: IRegister): Promise<any> {
    return api.post(API.PAYER.REGISTER, registerData);
  },
};
