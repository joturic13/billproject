import { API } from "../../constants/apiRoutes";
import { IStatistics } from "../../types/IStatistics";
import { api } from "../api";

export default {
  GetStatistics(): Promise<IStatistics> {
    return api.get(API.ADMIN.GET_STATISTICS);
  },
  GetPayerStatistics(): Promise<IStatistics> {
    return api.get(API.PAYER.GET_STATISTICS);
  },
};
