import { API } from "../../constants/apiRoutes";
import { ICreateExpense, IGetExpense } from "../../types/ICreateExpense";
import { ICreatePerson } from "../../types/IPerson";
import { api } from "../api";

export default {
  CreateExpense(expenseData: ICreateExpense): Promise<any> {
    return api.post(API.ADMIN.CREATE_EXPENSE, expenseData);
  },
  UpdateExpense(expenseData: IGetExpense): Promise<any> {
    return api.post(API.ADMIN.UPDATE_EXPENSE(expenseData.id), expenseData);
  },
  GetAllExpenses(): Promise<IGetExpense[]> {
    return api.get(API.ADMIN.GET_ALL_EXPENSES);
  },
  DeleteExpense(id: number): Promise<any> {
    return api.delete(API.ADMIN.DELETE_EXPENSE(id));
  },
  GetPersonalExpenses(): Promise<IGetExpense[]> {
    return api.get(API.PERSON_IN_NEED.GET_PERSONAL_EXPENSES);
  },
  CreatePersonalExpense(expenseData: ICreateExpense): Promise<any> {
    return api.post(API.PERSON_IN_NEED.CREATE_EXPENSE, expenseData);
  },
  GetExpensesToPay(): Promise<IGetExpense[]> {
    return api.get(API.PAYER.GET_ALL_EXPENSES);
  },
  PayExpense(id: number): Promise<any> {
    return api.put(API.PAYER.PAY_EXPENSE(id), { id: id });
  },
  FilterAdminExpenses(
    payedFilter: string,
    owner: string
  ): Promise<IGetExpense[]> {
    return api.get(API.ADMIN.FILTER_EXPENSES(payedFilter, owner));
  },
  FilterPersonInNeedExpenses(payedFilter: string): Promise<IGetExpense[]> {
    return api.get(API.PERSON_IN_NEED.FILTER_EXPENSES(payedFilter));
  },
  FilterPayerExpenses(owner: string): Promise<IGetExpense[]> {
    return api.get(API.PAYER.FILTER_EXPENSES(owner));
  },
  GetPayedExpensesd(): Promise<IGetExpense[]> {
    return api.get(API.PAYER.GET_PAYED_EXPENSES);
  },
};
