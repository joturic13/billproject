import React from "react";
import { Switch, Route, Router } from "react-router-dom";
import { createBrowserHistory } from "history";
import { APP } from "./constants/appRoutes";
import "./custom.css";
import { Store } from "./Store";
import PublicRoute from "./components/Routes/PublicRoute";
import SignUp from "./components/Homepage/SignUp/SignUp";
import SignIn from "./components/Homepage/SignIn/SignIn";
import ForgotPassword from "./components/Homepage/ForgotPassword/ForgotPassword";
import ResetPassword from "./components/Homepage/ResetPassword/ResetPassword";
import AdminDashboard from "./components/Roles/Admin/Dashboard/AdminDashboard";
import PrivateRoute from "./components/Routes/PrivateRoute";
import ListOfUsers from "./components/Roles/Admin/LIstOfUsers/ListOfUsers";
import ListOfExpenses from "./components/Roles/Admin/ListOfExpenses/ListOfExpenses";
import NewExpense from "./components/Roles/Admin/NewExpense/NewExpense";
import SnackbarConsumer from "./consumers/snackbarConsumer";
import ListOfPersonalExpenses from "./components/Roles/Person/ListOfExpenses/ListOfExpenses";
import NewPersonalExpense from "./components/Roles/Person/NewExpenses/NewExpense";
import ListOfExpensesToPay from "./components/Roles/Payer/ListOfExpenses/ListOfExpenses";
import UserProfile from "./components/Roles/Admin/Profile/UserProfile";
import ListOfPayedExpenses from "./components/Roles/Payer/PayedExpenses/PayedExpenses";
import UpdateExpense from "./components/Roles/Admin/UpdateExpense/UpdateExpense";
import UpdateUser from "./components/Roles/Admin/UpdateUser/UpdateUser";

const history = createBrowserHistory();

const App = () => {
  return (
    <Router history={history}>
      <script src="https://js.stripe.com/v3/" />
      <Store>
        <SnackbarConsumer />
        <Switch>
          <PublicRoute
            exact
            path={APP.HOMEPAGE}
            history={history}
            component={SignIn}
          />
          <PublicRoute
            exact
            path={APP.APP.REGISTER}
            history={history}
            component={SignUp}
          />
          <PublicRoute
            exact
            history={history}
            path={APP.APP.FORGOT_PASSWORD}
            component={ForgotPassword}
          />
          <PublicRoute
            exact
            history={history}
            path={APP.APP.RESET_PASSWORD}
            component={ResetPassword}
          />
          <PrivateRoute
            exact
            history={history}
            path={APP.ROLES.ADMIN.DASHBOARD}
            component={AdminDashboard}
          />
          <PrivateRoute
            exact
            history={history}
            path={APP.ROLES.ADMIN.CREATE_USER}
            component={UserProfile}
          />
          <PrivateRoute
            exact
            history={history}
            path={APP.ROLES.ADMIN.UPDATE_USER}
            component={UpdateUser}
          />
          <PrivateRoute
            exact
            history={history}
            path={APP.ROLES.ADMIN.UPDATE_EXPENSE}
            component={UpdateExpense}
          />
          <PrivateRoute
            exact
            history={history}
            path={APP.ROLES.PAYER.PAYED_EXPENSES}
            component={ListOfPayedExpenses}
          />
          <PrivateRoute
            exact
            history={history}
            path={APP.ROLES.PERSON_IN_NEED.LIST_OF_EXPENSES}
            component={ListOfPersonalExpenses}
          />
          <PrivateRoute
            exact
            history={history}
            path={APP.ROLES.PAYER.LIST_OF_EXPENSES}
            component={ListOfExpensesToPay}
          />
          <PrivateRoute
            exact
            history={history}
            path={APP.ROLES.PERSON_IN_NEED.NEW_EXPENSE}
            component={NewPersonalExpense}
          />
          <PrivateRoute
            exact
            history={history}
            path={APP.ROLES.ADMIN.LIST_OF_USERS}
            component={ListOfUsers}
          />
          <PrivateRoute
            exact
            history={history}
            path={APP.ROLES.ADMIN.LIST_OF_EXPENSES}
            component={ListOfExpenses}
          />
          <PrivateRoute
            exact
            history={history}
            path={APP.ROLES.ADMIN.NEW_EXPENSE}
            component={NewExpense}
          />
        </Switch>
      </Store>
    </Router>
  );
};

export default App;
