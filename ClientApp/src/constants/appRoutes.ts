import React from 'react'

export const APP = {
    HOMEPAGE: `/`,
    APP: {
       LOGIN: `/login`,
       REGISTER: `/register`,
       FORGOT_PASSWORD: `/forgot-password`,
       RESET_PASSWORD: `/reset-password`
    },
    ROLES: {
        ADMIN: {
            DASHBOARD: `/admin/dashboard`,
            LIST_OF_USERS: `/admin/list-of-users`,
            LIST_OF_EXPENSES: `/admin/list-of-expenses`,
            NEW_EXPENSE: `/admin/new-expense`,
            UPDATE_EXPENSE: `/admin/update-expense`,
            CREATE_USER: `/admin/create-user`,
            UPDATE_USER: `/admin/update-user`
        },
        PERSON_IN_NEED: {
            LIST_OF_EXPENSES: `/person-in-need/list-of-expenses`,
            NEW_EXPENSE: `/person-in-need/new-expense`
        },
        PAYER: {
            LIST_OF_EXPENSES: `/payer/list-of-expenses`,
            PAYED_EXPENSES: `/payer/list-of-payed-expenses`
        }
    }
}