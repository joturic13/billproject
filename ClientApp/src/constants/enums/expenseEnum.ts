import { IExpenseTypes } from "../../types/IExpenseTypes";

export const ExpenseEnum: IExpenseTypes[] = [{value: "1", name: 'Račun'}, 
 {value: "2", name: 'Stanarina'},
 {value: "3", name: 'Trošak kupovine'},
 {value: "4", name: 'Ostalo'}];