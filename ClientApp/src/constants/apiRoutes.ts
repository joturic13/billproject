import React from 'react'

export const API = {
    USER: {
        LOGIN: "Account/Login"
    },
    ADMIN: {
        CREATE_PERSON: "PersonInNeed",
        UPDATE_PERSON: (id: string) => `PersonInNeed/${id}`,
        GET_POPLE_IN_NEED: "PersonInNeed",
        DELETE_PERSON_IN_NEED : (id: string) => `PersonInNeed/${id}`,
        CREATE_EXPENSE: "Expense",
        UPDATE_EXPENSE: (id: number) => `Expense/${id}`,
        GET_ALL_EXPENSES: "Expense",
        DELETE_EXPENSE: (id: number) => `Expense/${id}`,
        GET_STATISTICS: `Statistics`,
        FILTER_EXPENSES: (payedFilter: string, expenseOwner: string) => `Expense/filter/admin/payed=${payedFilter}&expenseOWener=${expenseOwner}`
    },
    PERSON_IN_NEED: {
        GET_PERSONAL_EXPENSES: "Expense/my-expenses",
        CREATE_EXPENSE: "Expense/my-expenses",
        FILTER_EXPENSES: (payedFilter: string) => `Expense/filter/person-in-need/payed=${payedFilter}`
    },
    PAYER: {
        REGISTER: "Payer",
        GET_ALL_EXPENSES: "Expense/expenses-to-pay",
        PAY_EXPENSE: (id: number) => `Expense/${id}`,
        FILTER_EXPENSES: (expenseOwner: string) => `Expense/filter/payer/expenseOWener=${expenseOwner}`,
        GET_PAYED_EXPENSES: `Expense/payer/get-payed-expenses/`,
        GET_STATISTICS: `Statistics/payer`
    }
}