import React from "react";
import { ExpenseProvider } from "./providers/expense";
import { NotificationProvider } from "./providers/notification";
import { UserProvider } from "./providers/user";

export const Store = ({ children }: any) => {
  return (
    <NotificationProvider>
      <ExpenseProvider>
        <UserProvider>{children}</UserProvider>
      </ExpenseProvider>
    </NotificationProvider>
  );
};
