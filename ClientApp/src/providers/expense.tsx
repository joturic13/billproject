import { NightsStayTwoTone } from "@material-ui/icons";
import React, { useState } from "react";
import { IExpense, IGetExpense } from "../types/ICreateExpense";

const initExpense: IGetExpense = {
  id: -1,
  dateOfPayment: "",
  expenseAmount: -1,
  expenseDescription: "",
  expenseType: "",
  personInNeedId: "",
  payed: false,
  payer: {
    id: "",
    firstName: "",
    lastName: "",
    dateOfBirth: "",
    email: "",
    password: "",
    oib: "",
    address: "",
    iban: "",
    showPersonalData: false,
    familyNote: "",
    fullName: "",
  },
  personInNeed: {
    id: "",
    firstName: "",
    lastName: "",
    dateOfBirth: "",
    email: "",
    password: "",
    oib: "",
    address: "",
    iban: "",
    showPersonalData: false,
    familyNote: "",
    fullName: "",
  },
};

export const ExpenseContext = React.createContext<IExpense>({
  expense: {
    id: -1,
    dateOfPayment: "",
    expenseAmount: -1,
    expenseDescription: "",
    expenseType: "",
    personInNeedId: "",
    payed: false,
    payer: {
      id: "",
      firstName: "",
      lastName: "",
      dateOfBirth: "",
      email: "",
      password: "",
      oib: "",
      address: "",
      iban: "",
      showPersonalData: false,
      familyNote: "",
      fullName: "",
    },
    personInNeed: {
      id: "",
      firstName: "",
      lastName: "",
      dateOfBirth: "",
      email: "",
      password: "",
      oib: "",
      address: "",
      iban: "",
      showPersonalData: false,
      familyNote: "",
      fullName: "",
    },
  },
  setExpense: () => null,
});

export const ExpenseProvider: React.ComponentType<React.ReactNode> = (
  props
) => {
  const [expense, setExpense] = useState<IGetExpense>(initExpense);

  return (
    <ExpenseContext.Provider
      value={{
        expense,
        setExpense,
      }}
    >
      {props.children}
    </ExpenseContext.Provider>
  );
};
