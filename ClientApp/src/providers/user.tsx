import React, { useState } from "react";
import { ICreatePerson, IUser } from "../types/IPerson";

const initUser: ICreatePerson = {
  id: "",
  firstName: "",
  lastName: "",
  dateOfBirth: "",
  email: "",
  password: "",
  oib: "",
  address: "",
  iban: "",
  showPersonalData: false,
  familyNote: "",
  fullName: "",
};

export const UserContext = React.createContext<IUser>({
  user: initUser,
  setUser: () => null,
});

export const UserProvider: React.ComponentType<React.ReactNode> = (props) => {
  const [user, setUser] = useState<ICreatePerson>(initUser);

  return (
    <UserContext.Provider
      value={{
        user,
        setUser,
      }}
    >
      {props.children}
    </UserContext.Provider>
  );
};
